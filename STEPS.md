
# 01 - Create project
```
$ mix phx.new yabs

* creating yabs/config/config.exs
* creating yabs/config/dev.exs
* creating yabs/config/prod.exs
* creating yabs/config/prod.secret.exs
* creating yabs/config/test.exs
* creating yabs/lib/yabs/application.ex
* creating yabs/lib/yabs.ex
* creating yabs/lib/yabs_web/channels/user_socket.ex
* creating yabs/lib/yabs_web/views/error_helpers.ex
* creating yabs/lib/yabs_web/views/error_view.ex
* creating yabs/lib/yabs_web/endpoint.ex
* creating yabs/lib/yabs_web/router.ex
* creating yabs/lib/yabs_web.ex
* creating yabs/mix.exs
* creating yabs/README.md
* creating yabs/test/support/channel_case.ex
* creating yabs/test/support/conn_case.ex
* creating yabs/test/test_helper.exs
* creating yabs/test/yabs_web/views/error_view_test.exs
* creating yabs/lib/yabs_web/gettext.ex
* creating yabs/priv/gettext/en/LC_MESSAGES/errors.po
* creating yabs/priv/gettext/errors.pot
* creating yabs/lib/yabs/repo.ex
* creating yabs/priv/repo/seeds.exs
* creating yabs/test/support/data_case.ex
* creating yabs/lib/yabs_web/controllers/page_controller.ex
* creating yabs/lib/yabs_web/templates/layout/app.html.eex
* creating yabs/lib/yabs_web/templates/page/index.html.eex
* creating yabs/lib/yabs_web/views/layout_view.ex
* creating yabs/lib/yabs_web/views/page_view.ex
* creating yabs/test/yabs_web/controllers/page_controller_test.exs
* creating yabs/test/yabs_web/views/layout_view_test.exs
* creating yabs/test/yabs_web/views/page_view_test.exs
* creating yabs/.gitignore
* creating yabs/assets/brunch-config.js
* creating yabs/assets/css/app.css
* creating yabs/assets/css/phoenix.css
* creating yabs/assets/js/app.js
* creating yabs/assets/js/socket.js
* creating yabs/assets/package.json
* creating yabs/assets/static/robots.txt
* creating yabs/assets/static/images/phoenix.png
* creating yabs/assets/static/favicon.ico

Fetch and install dependencies? [Yn] Y
* running mix deps.get
* running mix deps.compile
* running cd assets && npm install && node node_modules/brunch/bin/brunch build

We are all set! Go into your application by running:

    $ cd yabs

Then configure your database in config/dev.exs and run:

    $ mix ecto.create

Start your Phoenix app with:

    $ mix phx.server

You can also run your app inside IEx (Interactive Elixir) as:

    $ iex -S mix phx.server
```

CD into your yabs folder `cd yabs` and initialize your git repository `git init`

Add `/.elixir_ls` to `.gitignore` file
